import {
	get,
	post
} from '../$http'
import store from '@/store/index.js'; //需要引入store
import axios from 'axios';

// 获取考试列表
export const getExamList = function(currentPage, name) {
	return post('/stuTheory/examCenter/myExam', {
		current:currentPage,
		"search": `{
			"name":'${name}'
		}`,
	}, store.state.access_token)
}
	
// 进入考试
export const enterExam = function(token, id) {
	return get('/stuTheory/examCenter/myExam/examination', {
		examId: id
	}, token)
}

// 考试答题
export const answerExam = function(answerObj) {
	return post('/stuTheory/examCenter/myExam/answer', {
		examRecordId: answerObj.examRecordId,
		paperQueId: answerObj.paperQueId,
		answer: answerObj.answer,
		template: answerObj.template
	}, store.state.access_token)
}

// 交卷
export const commitExam = function(examId, examRecordId) {
	return post('/stuTheory/examCenter/myExam/commitExam', {
		examId: examId,
		examRecordId: examRecordId
	}, store.state.access_token)
}

// 保存笔记
export const examNote = function(qId, contents) {
	return post('/stuTheory/examCenter/myResult/addNotes', {
		id: qId,
		comment: contents
	}, store.state.access_token)
}

// 批量删除笔记
export const delBatch = function(selectStr) {
	return post('/stuTheory/myNote/delBatch', {
		ids: selectStr
	}, store.state.access_token)
}

// 获取成绩列表
export const getGradeList = function(currentPage,ExamName) {
	return post('/stuTheory/examCenter/myResult', {
		current: currentPage,
		"search": `{
			"name":'${ExamName}'
		}`,
	}, store.state.access_token)
}

// 查看成绩
export const checkGrade = function(recordId) {
	return get('/stuTheory/examCenter/myResult/check', {
		recordId: recordId
	}, store.state.access_token)
}

// 知识点练习
export const getChapter = function(params) {
	return get('/stuTheory/exerciseCenter/chapter', params, store.state.access_token)
}

// 重置知识点练习
export const reSetKnow = function(params) {
	return get('/stuTheory/exerciseCenter/chapter/reset', params, store.state.access_token)
}

// 开始知识点练习
export const startKnow = function(knowSearch) {
	return post('stuTheory/exerciseCenter/chapter/start', {
		// search: `{
		// 	"storeIds":${knowSearch.storeIds},
		// "subjectIds":${knowSearch.subjectIds}},
		// "examPointIds":${knowSearch.examPointIds},
		// "queType":"{'1':'1'}"
		// }`,
		"search": `{
			"storeIds":${knowSearch.storeIds},
			"queType":'{"1":${knowSearch.queType.single},"2":${knowSearch.queType.multi},"3":${knowSearch.queType.judge},"4":${knowSearch.queType.fill},"5":${knowSearch.queType.mess}}'
		}`
		// "subjectIds":${knowSearch.subjectIds}},
		// "queType":'{"1":${knowSearch.queType.single},
		// "2":${knowSearch.queType.multi},
		// "3":"0","4":"0","5":"0"}'
		// "search": `{
		// 	"storeIds":"335604942683456",
		// 	"queType":"{\\\"1\\\":\\\"1\\\",\\\"2\\\":\\\"1\\\",\\\"3\\\":\\\"1\\\",\\\"4\\\":\\\"0\\\",\\\"5\\\":\\\"0\\\"}"
		// }`

	}, store.state.access_token)
}

// 知识点与模拟考试答题
export const answerPaper = function(answerObj) {
	return post('/stuTheory/exerciseCenter/chapter/answer', {
		exerciseId: answerObj.exerciseId,
		paperQueId: answerObj.paperQueId,
		answer: answerObj.answer,
		template: answerObj.template
	}, store.state.access_token)
}

// 知识点与模拟考试交卷
export const commitPaper = function(exerciseId) {
	return post('/stuTheory/exerciseCenter/chapter/commit', {
		exerciseId: exerciseId
	}, store.state.access_token)
}

// 知识点与模拟考试交卷结果
export const paperResult = function(exerciseId) {
	return get('/stuTheory/exerciseCenter/chapter/finishExercise', {
		exerciseId: exerciseId
	}, store.state.access_token)
}


// 获取题目数量
export const getQuestionSum = function(params) {
	return post('/stuTheory/exerciseCenter/chapter/getQuestionSum', params, store.state.access_token)
}

// 获取题库
export const getBank = function(currentPage,name) {
	return post('/stuTheory/exerciseCenter/store', {
		current: currentPage,
		"search": `{
			"name":'${name}'
		}`,
	}, store.state.access_token)
}

// 获取题库题目
export const getBankQues = function(sId) {
	return post('/stuTheory/exerciseCenter/store/enter', {
		search: `{"storeId":${sId}}`
	}, store.state.access_token)
}

// 开始题库练习
export const startBankExercise = function(sId, quet) {
	return get(`/stuTheory/exerciseCenter/store/startExercise`, {
		search: `{"storeId":${sId},"queType":${quet}}`,
		// storeId: storeId
	}, store.state.access_token)
}

// 保存题库练习记录
export const storeSave = function(qId, sId) {
	return post('/stuTheory/exerciseCenter/store/save', {
		queId: qId,
		storeId: sId
	}, store.state.access_token)
}

// 重置题库
export const resetExecise = function(qType, sId) {
	return get('/stuTheory/exerciseCenter/store/resetQuetype', {
		queType: qType,
		storeId: sId
	}, store.state.access_token)
}

// 查询模拟考试
export const paper = function() {
	return get(`/stuTheory/exerciseCenter/paper`, {}, store.state.access_token)
}

// 条件查询模拟考试
// export const queryPaper = function(subjectId,name) {
// 	return post(`/stuTheory/exerciseCenter/paper`, {
// 		search :`{"subjectId":${subjectId},"name":${name}}`
// 	}, store.state.access_token)
// }
export const queryPaper = function(currentPage,name) {
	return post(`/stuTheory/exerciseCenter/paper`, {
		current: currentPage,
				"search": `{
					"name":'${name}'
				}`,
	}, store.state.access_token)
}

// 开始模拟考试
export const paperStart = function(paperId) {
	return get(`/stuTheory/exerciseCenter/paper/start`, {
		id: paperId
	}, store.state.access_token)
}

// 获取错题列表
export const getErrorList = function(currentPage,queContent,template,queDifficulty) {
	return post(`/stuTheory/myErrorQue`, {
		current: currentPage,
		"search": `{
			"queContent":'${queContent}',
			"template":'${template}',
			"queDifficulty":'${queDifficulty}'
		}`,
	}, store.state.access_token)
}
// 开始错题练习
export const startErrorPrac = function(template,queDifficulty) {
	return get(`/stuTheory/myErrorQue/errorTraining`, {
		"search": `{
			"template":'${template}',
			"queDifficulty":'${queDifficulty}'
		}`,
			// "queContent":'${queContent}',
	}, store.state.access_token)
}

// 获取错题的详情
export const getErrorDetail = function(token, params) {
	return get(`/stuTheory/myErrorQue/errorTraining`, {
		// id: id
	}, token)
}

// 获取错题的题目详情
export const errorQueDetail = function(id) {
	return get(`/stuTheory/myErrorQue/check`, {
		id: id
	}, store.state.access_token)
}

// 保存笔记
export const addNote = function(id,content) {
	return post(`/stuTheory/myErrorQue/addNote`, {
		queId: id,
		content:content
	}, store.state.access_token)
}

// 获取笔记列表
export const getNotesList = function(currentPage, noteContent,queContent) {
	return post(`/stuTheory/myNote`, {
		current: currentPage,
		"search": `{
			"queContent":'${queContent}',
			"noteContent":'${noteContent}'
		}`,
	}, store.state.access_token)
}

// 更新笔记
export const updateNote = function(noteId,noteContent) {
	return post(`/stuTheory/myNote/update`, {
		id: noteId,
		content: noteContent
	}, store.state.access_token)
}

// 获取错题的题目详情
export const noteQueDetail = function(id) {
	return get(`/stuTheory/myNote/check`, {
		id: id
	}, store.state.access_token)
}

// 练习记录列表
export const pracRecord = function(currentPage, exeType) {
	return post(`/stuTheory/exerciseCenter/result`, {
		current: currentPage,
		"search": `{
			"exerciseType":'${exeType}'
		}`,
	}, store.state.access_token)
}

// 练习记录详情
export const pracRecordDetail = function(recordId) {
	return get(`/stuTheory/exerciseCenter/result/check`, {
		id : recordId,
	}, store.state.access_token)
}

// 批量删除练习记录
export const delRecord = function(selectStr) {
	return post('/stuTheory/exerciseCenter/result/delBatch', {
		ids: selectStr
	}, store.state.access_token)
}
// 人脸识别请求
export const checkFace = function(picData) {
	return post_pic(`/system/checkFace`, {
		pic: picData
	}, store.state.access_token)
}
