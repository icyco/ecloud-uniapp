import {
	get,
	post
} from '../$http'

export const getMyCourseParams = function(token, params) {
	return get('/stuCourse/myCourse', params, token)
}

export const getMyCourseList = function(token, params) {
	return post('/stuCourse/myCourse', params, token)
}

export const getCourseInfo = function(token, id) {
	return get('/stuCourse/myCourse/lesson', {
		courseId: id
	}, token)
}

export const getStudyQues = function(token, id, qaNum, qaPage) {
	return post('/stuCourse/myCourse/study/getStudyQues', {
		search: `{"courseId":${id}}`,
		limit: qaNum,
		current: qaPage
	}, token)
}

export const getStudyChapter = function(token, id) {
	return post('/stuCourse/myCourse/study', {
		courseId: id
	}, token)
}

export const getOpenCourseParams = function(token, params) {
	return get('/stuCourse/openCourse', params, token)
}

export const getOpenCourseList = function(token, params) {
	return post('/stuCourse/openCourse', params, token)
}

export const getOpenCourseInfo = function(token, id) {
	return get('/stuCourse/openCourse/lesson', {
		courseId: id
	}, token)
}

export const getStudyStatus = function(token, Id) {
	return post('/stuCourse/myCourse/study/getStudyStatus', {
		chapterId: Id
	}, token)
}

export const showTag = function(token, id) {
	return get('/stuCourse/myCourse/study/showTag', {
		designId: id
	}, token)
}

export const getStudyQuestions = function(token, id, My, qaNum, qaPage) {
	return post('/stuCourse/myCourse/study/getStudyQuestions', {
		search: `{"isMy":${My},"designId":${id}}`,
		limit: qaNum,
		current: qaPage
	}, token)
}

export const delQuestions = function(token, queid) {
	return post('/stuCourse/myCourse/study/delQuestions', {
		id: queid
	}, token)
}

export const updateQuestions = function(token, queid, queTitle, queContent) {
	return post('/stuCourse/myCourse/study/getStudyQuestions', {
		id: queid,
		title: queTitle,
		content: queContent
	}, token)
}

export const addQuestions = function(token, leftid, quefamilyId, queContent, queTitle) {
	return post('/stuCourse/myCourse/study/addQuestions', {
		designId: leftid,
		fatherId: quefamilyId,
		content: queContent,
		title: queTitle
	}, token)
}

export const getStudyNotes = function(token, id, noteNum, notePage) {
	return post('/stuCourse/myCourse/study/getStudyNotes', {
		search: `{"designId":${id}}`,
		limit: noteNum,
		current: notePage
	}, token)
}

export const addNotes = function(token, leftid, noteContent) {
	return post('/stuCourse/myCourse/study/addNotes', {
		designId: leftid,
		content: noteContent
	}, token)
}

export const getStudyResourceResult = function(token, id, Num, Page) {
	return post('/stuCourse/myCourse/study/getStudyResourceResult', {
		search: `{"designId":${id}}`,
		limit: Num,
		current: Page
	}, token)
}

export const addTheoryRecord = function(token, leftid) {
	return post('/stuCourse/myCourse/study/addTheoryRecord', {
		designId: leftid
	}, token)
}

export const startTheory = function(token, pracid) {
	return get('/stuCourse/myCourse/study/startTheory', {
		id: pracid
	}, token)
}

export const answerCourse = function(token, answerObj) {
	return post('/stuCourse/myCourse/study/answerTheory', {
		exerciseId: answerObj.exerciseId,
		paperQueId: answerObj.paperQueId,
		answer: answerObj.answer,
		template: answerObj.template
	}, token)
}

export const commitCourse = function(token, exerciseId) {
	return post('/stuCourse/myCourse/study/commitTheory', {
		exerciseId: exerciseId
	}, token)
}

export const checkTheory = function(token, pracid) {
	return get('/stuCourse/myCourse/study/checkTheory', {
		id: pracid
	}, token)
}

export const uploadAnswer = function(token, Id, answerText) {
	return post('/stuCourse/myCourse/study/uploadAnswer', {
		search: `{"designId":${Id},"answer":${answerText}}`
	}, token)
}
