import {
	get,
	post
} from '../$http'

export const getMyNote = function(token, params) {
	return post('/stuCourse/myNote', params, token)
}

export const delNoteBatch = function(token, id) {
	return post('/stuCourse/myNote/delBatch', {
		ids: id
	}, token)
}

export const updateNotes = function(token, id, content) {
	return post('/stuCourse/myNote/updateNotes', {
		id: id,
		content: content
	}, token)
}
