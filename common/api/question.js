import {
	get,
	post
} from '../$http'

export const getMyQuestionList = function(token, search, num, page) {
	return post('/stuCourse/myQuestion', {
		search: search,
		limit: num,
		current: page
	}, token)
}

export const getMyQuestionAnswer = function(token, id) {
	return get('/stuCourse/myQuestion/answerQuestion', {
		queId: id
	}, token)
}

export const updateQuestions = function(token, id, title, content) {
	return post('/stuCourse/myQuestion/updateQuestions', {
		id: id,
		title: title,
		content: content
	}, token)
}

export const delQuestionsBatch = function(token, id) {
	return post('/stuCourse/myQuestion/delBatch', {
		ids: id,
	}, token)
}

export const addQuestions = function(token, designId, fatherId, content, title) {
	return post('/stuCourse/myQuestion/addQuestions', {
		designId: designId,
		fatherId: fatherId,
		content: content,
		title: title
	}, token)
}
