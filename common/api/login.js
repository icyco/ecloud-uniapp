import {
	get,
	post
} from '../$http'
import store from '@/store/index.js'; //需要引入store
import axios from 'axios'



export const encrypt = function(input) {
	var keyStr = "ABCDEFGHIJKLMNOP" + "QRSTUVWXYZabcdef" + "ghijklmnopqrstuv" +
		"wxyz0123456789+/" + "=";
	var output = "";
	var chr1, chr2, chr3 = "";
	var enc1, enc2, enc3, enc4 = "";
	var i = 0;
	do {
		chr1 = input.charCodeAt(i++);
		chr2 = input.charCodeAt(i++);
		chr3 = input.charCodeAt(i++);
		enc1 = chr1 >> 2;
		enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
		enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
		enc4 = chr3 & 63;
		if (isNaN(chr2)) {
			enc3 = enc4 = 64;
		} else if (isNaN(chr3)) {
			enc4 = 64;
		}
		output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) +
			keyStr.charAt(enc3) + keyStr.charAt(enc4);
		chr1 = chr2 = chr3 = "";
		enc1 = enc2 = enc3 = enc4 = "";
	} while (i < input.length);
	return output;
}

// 获取token
export const login = function(account, password) {
	let enPassword = encrypt(new Date().getTime() + password)
	return get('/stuLogin/login', {
		studentNo: account,
		password: enPassword
	})
}

// 获取学生信息
export const getStuInfo = function() {
	return get('/system/userMessage', {
	}, store.state.access_token)
}