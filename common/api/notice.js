import {
	get,
	post
} from '../$http'

export const getMyNotice = function(token, search) {
	return post('/stuNotice/myNotice', search, token)
}

export const deleteBatchNotice = function(token, ids) {
	return post('/stuNotice/myNotice/deleteBatch', ids, token)
}

export const checkNotice = function(token, id) {
	return post('/stuNotice/myNotice/check', id, token)
}
