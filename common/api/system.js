import {
	get,
	post
} from '../$http'

export const download = function(accessToken, downloadToken) {
	return get('/resource/download', {
		token: downloadToken,
		accessToken: accessToken
	}, accessToken)
}
