import store from '@/store/index.js'; //需要引入store
export const getTokenStorage = function() {
	uni.getStorage({ //获得保存在本地的用户信息
		key: 'token',
		success: (res) => {
			console.log('获取本地存储成功');
			console.log('token_store res', res);
			store.commit('setAccessToken', res.data)
			// store.state.hasLogin = true
			// this.login(res.data);
		},
		fail() {
			console.log('获取本地存储失败');
			uni.showToast({
				title: '请在登录后查看！',
				duration: 2000
			})
			setTimeout(function() {
				uni.navigateTo({
					url: '@/pages/login/login'
				})
			}, 2000);

		}
	})
}
