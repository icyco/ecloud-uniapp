const myExamList = {
	"total": 2, //…………………………总数 
	"size": 10, //…………………………每页条数 
	"pages": 1, //…………………………总页数 
	"current": 1, //…………………………当前页 
	"records": [ //…………………………理论考试集合 
		/*
			status == 1 未开始
			status == 2 已开始
			status == 3 已结束
		*/
		{
			"id": "3301",
			"name": "微观经济学",
			"maxScore": "100",
			"examStartTime": "2023-01-21 08:00:00",
			"examEndTime": "2023-01-31 23:00:00",
			"subjectId": "311002600611136",
			"examTime": -1,
			"status": 1,
			"studentExamStatus": 1,
			"canCheckGrade": false,
			"canStartExam": true,
			"checkFace": 1
		},
		{
			"id": "3302",
			"name": "宏观经济学",
			"maxScore": "90",
			"examStartTime": "2023-01-1 08:00:00",
			"examEndTime": "2023-01-11 23:00:00",
			"subjectId": "311002600611136",
			"examTime": 120,
			"status": 3,
			"studentExamStatus": 4,
			"canCheckGrade": true,
			"canStartExam": true,
			"checkFace": 1
		},
		{
			"id": "3303",
			"name": "计算机基础",
			"maxScore": "120",
			"examStartTime": "2022-12-1 08:00:00",
			"examEndTime": "2022-12-11 23:00:00",
			"subjectId": "311002600611136",
			"examTime": 100,
			"status": 2,
			"studentExamStatus": 4,
			"canCheckGrade": false,
			"canStartExam": true,
			"checkFace": 2
		}
	]
}
module.exports = {
	myExamList: myExamList //前一个userInfo是将后一个userInfo对象数组暴露出去的命名的名字，自己定义的
}
