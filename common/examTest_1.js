const examTest1 = {
	"exam": { // //考试信息
		"id": "3301", // 主键
		"name": "微观经济学", // 名称
		"type": "exam",
		"examTime": -1, //考试时长(min，-1代表不限时长)
		"maxScore": 100.0, //总分
		"seeAnswer": 1, //1交卷后查看 2做题中及交卷后查看 3不允许查看
		"totalQuestion": 5 //总题数
	},
	"student": { //学生信息
		"id": "310367385200960", //学生主键
		"studentNo": "2020001", //学号
		"realName": "李易峰", //姓名
	},
	"clazz": { //班级信息
		"id": "310367137296704", //主键
		"name": "财务1班" //名称
	},
	"recordId": "331705410435392", //考试记录主键
	"time": "-1", //剩余时间(ms) -1没有时间限制
	"questionList": [
		{ //第一题, 单选题
			"id": 331705394020672, //试题主键
			"queIndex": 1,
			"queScore": 2.0, //分值
			"queTypeName": "单选题", //题型
			"queType": 1, // 题型: 1单选 2多选 3判断 4填空 5解答 代替template
			"template": 1, //
			"queDifficulty": 1, //难度:1容易 2中等 3困难
			"queAnalysis": "", //解析

			"queContent": "经济资源与非经济资源的区别主要在于（ ）", //题干
			"options": [{ // 单选题第一个选项信息
				"id": "331705394027840", //主键
				"optContent": "资源的位置", //内容 648+328+128 = 1104
				"optLetter": "A", //选项(A,B,C,D……)，答题时传参数时使用
				"newOptLetter": "A", //新选项(A,B,C,D……)，页面显示作用，无实际意义
			}, { // 单选题第二个选项信息
				"id": "331705394027841", //主键
				"optContent": "资源的性质", //内容 648+328+128 = 1104
				"optLetter": "B", //选项(A,B,C,D……)，答题时传参数时使用
				"newOptLetter": "B", //新选项(A,B,C,D……)，页面显示作用，无实际意义
			}, { // 单选题第三个选项信息
				"id": "331705394027842", //主键
				"optContent": "资源的来源", //内容 648+328+128 = 1104
				"optLetter": "C", //选项(A,B,C,D……)，答题时传参数时使用
				"newOptLetter": "C", //新选项(A,B,C,D……)，页面显示作用，无实际意义
			}, { // 单选题第四个选项信息
				"id": "331705394027843", //主键
				"optContent": "资源的去向", //内容 648+328+128 = 1104
				"optLetter": "D", //选项(A,B,C,D……)，答题时传参数时使用
				"newOptLetter": "D", //新选项(A,B,C,D……)，页面显示作用，无实际意义
			}],
			"tempAnswer": "", // 记录学生作答时的暂时答案

			// 以下是Review状态下所需要的参数
			"studentAns": "", // 学生的最终提交答案
			"rightAns": "", // 本题的正确答案
			"isRight": "", // 学生是否回答正确: Y正确 N错误 U无正误判断
			"wrongArr":"", // 记录学生的错误答案
		},
		{ //第二题, 多选题
			"id": 331705394030912,
			"queScore": 4.0,
			"queTypeName": "多选题",
			"queType": 2,
			"template": 2,
			"queDifficulty": 2, //难度:1容易 2中等 3困难
			"queAnalysis": "", //解析

			"queContent": "经济学分为（）",
			"options": [{
				"id": "331705394032960",
				"optContent": "微观经济学",
				"optLetter": "A",
			}, {
				"id": "331705394032961",
				"optContent": "中观经济学",
				"optLetter": "B",
			}, {
				"id": "331705394032962",
				"optContent": "宏观经济学",
				"optLetter": "C",
			}, {
				"id": "331705394032963",
				"optContent": "国民经济学",
				"optLetter": "D",
			}],
			"tempAnswer": "", // 记录学生作答时的暂时答案

			// 以下是Review状态下所需要的参数
			"studentAns": "", // 学生的最终提交答案
			"rightAns": "", // 本题的正确答案
			"isRight": "", // 学生是否回答正确: Y正确 N错误 U无正误判断
			"wrongArr":"", // 记录学生的错误答案
		},
		{ //第三题, 判断题
			"id": 331705394037056,
			"queScore": 1.0,
			"queTypeName": "判断题",
			"queType": 3,
			"template": 3,
			"queDifficulty": 1,
			"queAnalysis": "",

			"queContent": "如果x与y商品是互补品，x价格下降，将使价格下降",
			"options": [{
				"id": "331705394044221",
				"optContent": "正确",
				"optLetter": "Y",
			}, {
				"id": "331705394044222",
				"optContent": "错误",
				"optLetter": "N",
			}],
			"tempAnswer": "", // 记录学生作答时的暂时答案

			// 以下是Review状态下所需要的参数
			"studentAns": "", // 学生的最终提交答案
			"rightAns": "", // 本题的正确答案
			"isRight": "", // 学生是否回答正确: Y正确 N错误 U无正误判断
			"wrongArr":"", // 记录学生的错误答案
		},
		{ //第四题, 填空题
			"id": 331705394045248,
			"queScore": 3.0,
			"queTypeName": "填空题",
			"queType": 4,
			"template": 4,
			"queDifficulty": 1,
			"queAnalysis": "",

			"queContent": "消费者预算线发生平移时 连接消费者平衡点的曲线称为______",
			"options": [{ // 填空题有几个空就有几个option
				"id": "331705394050361",
			}],
			"tempAnswer": "", // 记录学生作答时的暂时答案

			// 以下是Review状态下所需要的参数
			"studentAns": "", // 学生的最终提交答案
			"rightAns": "", // 本题的正确答案
			"isRight": "", // 学生是否回答正确: Y正确 N错误 U无正误判断
			"wrongArr":"", // 记录学生的错误答案
		},
		{ //第五题, 简答题
			"id": 331705394052416,
			"queScore": 10.0,
			"queTypeName": "简答题",
			"queType": 5,
			"template": 5,
			"queDifficulty": 1,
			"queAnalysis": "",

			"queContent": "请详细描述供需定理",
			"tempAnswer": "", // 记录学生作答时的暂时答案

			// 以下是Review状态下所需要的参数
			"studentAns": "", // 学生的最终提交答案
			"rightAns": "", // 本题的正确答案
			"isRight": "", // 学生是否回答正确: Y正确 N错误 U无正误判断
			"wrongArr":"", // 记录学生的错误答案
		}
	]
}

module.exports = {
	examTest1: examTest1 //前一个userInfo是将后一个userInfo对象数组暴露出去的命名的名字，自己定义的
}
