import axios from 'axios'
import axiosAdapterUniapp from 'axios-adapter-uniapp'
import qs from 'qs'

axios.defaults.baseURL = "http://61.132.233.226:9001/escloud/ws2";
axios.defaults.timeout = 10000
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
axios.defaults.adapter = axiosAdapterUniapp

export function get(url, params, token) {
	return new Promise((resolve, reject) => {
		axios.get(url, {
			params: params,
			headers: {
				"Authorization": "Bearer " + token
			}
		}).then(res => {
			resolve(res.data)
		}).catch(err => {
			reject(err.data)
		})
	})
}

export function post(url, data, token) {
	let postData = qs.stringify(data)
	return new Promise((resolve, reject) => {
		axios({
				method: 'post',
				url: url,
				data: postData,
				headers: {
					"Authorization": "Bearer " + token,
					"Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
				}
			})
			.then(res => {
				resolve(res.data)
			})
			.catch(err => {
				reject(err.data)
			})
	})
}