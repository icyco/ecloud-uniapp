export default [
	// 第一题，单选题
	{
		"id": 541,
		"title": "经济学中的“稀缺性”是指（）。",
		"answer": "B",
		"theoryExamAnswer": "",
		"type": 1,
		"difficulty": "易",
		"explainContent": "经济学中的稀缺性是指相对于资源的需求而言，资源总是不足的",
		"noteContent":{
			"noteTitle": "经济学中的稀缺性",
			"noteContents": "经济学中的稀缺性是指相对于资源的需求而言，资源总是不足的",
			"noteTime": "2021-01-21 16:00"
		},
		"optionList": [{
				"optionId": 2045,
				"id": "A",
				"questionId": 541,
				"content": "世界上大多数人生活在贫困中",
				"type": 1,
			},
			{
				"optionId": 2046,
				"id": "B",
				"questionId": 541,
				"content": "相对于资源的需求而言，资源总是不足的",
				"type": 1,
			},
			{
				"optionId": 2047,
				"id": "C",
				"questionId": 541,
				"content": "用资源必须考虑下一代",
				"type": 1,
			},
			{
				"optionId": 2048,
				"id": "D",
				"questionId": 541,
				"content": "世界上的资源终将被人类消耗光",
				"type": 1,
			}
		],
	},
	// 第二题，多选题
	{
		"id": 685,
		"title": "市场均衡分为（）",
		"answer": "AC",
		"theoryExamAnswer": "",
		"type": 2,
		"difficulty": "中",
		"explainContent": "市场均衡分为（）",
		"noteContent":{},
		"optionList": [{
				"optionId": 2543,
				"id": "A",
				"questionId": 685,
				"content": "局部均衡",
				"type": 3,
			},
			{
				"optionId": 2544,
				"id": "B",
				"questionId": 685,
				"content": "特殊均衡",
				"type": 3,
			},
			{
				"optionId": 2545,
				"id": "C",
				"questionId": 685,
				"content": "一般均衡",
				"type": 3,
			},
			{
				"optionId": 2546,
				"id": "D",
				"questionId": 685,
				"content": "整体均衡",
				"type": 3,
			}
		],
	},
	// 第三题 判断题
	{
			"id": 652,
			"title": "看不见的手一般指价格",
			"answer": "A",
			"theoryExamAnswer": "",
			"type": 3,
			"difficulty": "易",
			"explainContent": "经济学中，看不见的手一般指价格",
			"noteContent":{
				"noteTitle": "看不见的手",
				"noteContents": "一定要记得，经济学中，看不见的手一般指价格",
				"noteTime": "2021-01-20 16:00"
			},
			"optionList": [{
					"optionId": 2469,
					"id": "A",
					"questionId": 652,
					"content": "对",
					"type": 3,
				},
				{
					"optionId": 2470,
					"id": "B",
					"questionId": 652,
					"content": "错",
					"type": 3,
				}
			],
		},
	
	// 第四题 填空题
	{
			"id": 652,
			"title": "消费者预算线发生平移时，连接消费者均衡点的曲线称为________",
			"answer": "收入－消费曲线",
			"theoryExamAnswer": "",
			"type": 4,
			"difficulty": "中",
			"explainContent": "消费者预算线发生平移时，连接消费者均衡点的曲线称为收入－消费曲线",
			"noteContent":{
				"noteTitle": "收入－消费曲线",
				"noteContents": "一定要记得，消费者预算线发生平移时，连接消费者均衡点的曲线称为收入－消费曲线",
				"noteTime": "2021-01-20 16:00"
			},
		},
		// 第五题 简答题
		{
				"id": 652,
				"title": "请对“供求定理”这一名词进行解释",
				"answer": `供求变动对均衡价格和均衡产量的影响。需求的变动引起均衡价格和均衡产量同方向变动；供给的变动引起均衡价格反方向变动，均衡产量同方向变动。`,
				"theoryExamAnswer": "",
				"type": 5,
				"difficulty": "中",
				"explainContent": `供求变动对均衡价格和均衡产量的影响。需求的变动引起均衡价格和均衡产量同方向变动；供给的变动引起均衡价格反方向变动，均衡产量同方向变动。`,
				"noteContent":{
					"noteTitle": "供求定理",
					"noteContents": `一定要记得，供求变动对均衡价格和均衡产量的影响。需求的变动引起均衡价格和均衡产量同方向变动；供给的变动引起均衡价格反方向变动，均衡产量同方向变动。`,
					"noteTime": "2021-01-20 16:00"
				},
			},
]
